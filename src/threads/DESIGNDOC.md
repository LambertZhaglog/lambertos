

# ALARM CLOCK

## DATA STRUCTURES

>A1: Copy here the declaration of each new or changed `struct` or `struct` member, global or static variable, `typedef`, or enumeration.  Identify the purpose of each in 25 words or less.

```c
struct sleeping_thread{
  struct list_elem elem;
  struct thread *thread;
  int64_t ticks_to_wake;                             /* Wake the sleeping thread at the right time */
};

struct list sleeping_list;                           /* Sorted by sleeping_thread.ticks_to_wake in ascending order */
struct sleeping_thread fake_sleeping_thread;         /* A trick */
static int64_t next_wake_tick;                       /* Another trick */
```

## ALGORITHMS

>A2: Briefly describe what happens in a call to timer_sleep(), including the effects of the timer interrupt handler.

- when a thread call timer_sleep(), he will first record the sleeping information in struct sleeping_thread, then add it to proper position in sleeping_list.
- every time a timer interrupt occur, the handler check the sleeping_list and wake up threads those should get up.

>A3: What steps are taken to minimize the amount of time spent in
>the timer interrupt handler?

I use a fake thread to emit the overhead of check list_empty(&sleeping_thread)
and use a global variable to minimize the time of check next waken up thread's ticks_to_wake

## SYNCHRONIZATION

>A4: How are race conditions avoided when multiple threads call timer_sleep() simultaneously?

when we add a sleeping thread to sleeping_list, we close the interrupt first that only one thread can modify the sleeping_list one time.

>A5: How are race conditions avoided when a timer interrupt occurs during a call to timer_sleep()?

both timer_sleep and timer interrupt handler modify the sleeping_list, but they all close interrupt first before their operation

## RATIONALE

>A6: Why did you choose this design?  In what ways is it superior to another design you considered?

# PRIORITY SCHEDULING

## DATA STRUCTURES

>B1: Copy here the declaration of each new or changed `struct` or `struct` member, global or static variable, `typedef`, or enumeration.  Identify the purpose of each in 25 words or less.

```c
// add to struct thread
/* two entry below is used to implement scheduler */
struct list locks_holding;          /* Locks that current thread hold */
struct lock *lock_waiting;          /* Lock that this thread blocked by */
int effective_priority;             /* Priorirty used by schedule policy, such as priority schedule with priority donation, or mlfq scheduler */

// add to struct lock
struct list_elem elem;      /* List element of thread.locks_holding */
```


>B2: Explain the data structure used to track priority donation. Use ASCII art to diagram a nested donation.  (Alternately, submit a .png file.)

use the locks_holding, lock_waiting to construct a lock_acquire_block tree

## ALGORITHMS

>B3: How do you ensure that the highest priority thread waiting for a lock, semaphore, or condition variable wakes up first?

before wake up thread, I fisrt search the highest

```c
struct thread *get_highest_effective_priority_thread(struct list *list);
struct thread *pop_highest_effective_priority_thread(struct list *list);
```


>B4: Describe the sequence of events when a call to lock_acquire() causes a priority donation.  How is nested donation handled?

I reimplement the lock_acquire function, not use the sema_down function.
the core donate handle as follows:

```c
  while(sema->value==0){
    cur->lock_waiting=lock;
    if(thread_mlfqs==false)
      donate_priority(lock,cur->effective_priority);
    list_push_back(&sema->waiters,&cur->elem);
    thread_block();
    cur->lock_waiting=NULL;
  }
```

where the function donate_priority is a recursive function which traverse the lock_acquire_block tree

>B5: Describe the sequence of events when lock_release() is called on a lock that a higher-priority thread is waiting for.

first wake up the highest-priority thread, then thread_yield()

## SYNCHRONIZATION

>B6: Describe a potential race in thread_set_priority() and explain how your implementation avoids it. Can you use a lock to avoid
>this race?

close the interrupt

## RATIONALE

>B7: Why did you choose this design?  In what ways is it superior to another design you considered?

# ADVANCED SCHEDULER

## DATA STRUCTURES

>C1:Copy here the declaration of each new or changed `struct` or `struct` member, global or static variable, `typedef`, or enumeration.  Identify the purpose of each in 25 words or less.

```c
// add to thread.h
typedef int32_t fp_t;
// add to thread
		fp_t recent_cpu;
    int nice;
    struct list_elem allelem;           /* List element for all threads list. */
// add to thread.c
fp_t load_avg;
```

## ALGORITHMS

>C2: Suppose threads A, B, and C have nice values 0, 1, and 2.  Each has a recent_cpu value of 0.  Fill in the table below showing the scheduling decision and the priority and recent_cpu values for each thread after each given number of timer ticks:

```
timer  recent_cpu    priority   thread
ticks   A   B   C   A   B   C   to run
-----  --  --  --  --  --  --   ------
 0     0   0   0   63  61  59	A
 4     4   0   0   62  61  59	A
 8     8   0   0   61  61  59	B
12     8   4   0   61  60  59	A
16     12  4   0   60  60  59	B
20     12  8   0   60  59  59	A
24     16  8   0   59  59  59	B
28     16  12  0   59  58  59	C
32     16  12  4   59  58  58	A
36     20  12  4   58  58  58	B
```

>C3: Did any ambiguities in the scheduler specification make values in the table uncertain?  If so, what rule did you use to resolve them?  Does this match the behavior of your scheduler?

>C4: How is the way you divided the cost of scheduling between code inside and outside interrupt context likely to affect performance?

## RATIONALE

>C5: Briefly critique your design, pointing out advantages and disadvantages in your design choices.  If you were to have extra time to work on this part of the project, how might you choose to refine or improve your design?

# 笔记

Thread block 是把当前线程加入wait list，并thread Switch

thread unblock 是把线程从wait list 移入 ready list